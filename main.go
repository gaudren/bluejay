package main

import (
	"log"
	"os"
	"fmt"
	"github.com/nanu-c/qml-go"
	"github.com/coldbrewcloud/go-shippo"
	//"github.com/coldbrewcloud/go-shippo/models"
)

func main() {
	os.Setenv("QT_QUICK_CONTROLS_STYLE", "Material")
	os.Setenv("QT_AUTO_SCREEN_SCALE_FACTOR", "1")
	err := qml.Run(run)
	if err != nil {
		log.Fatal(err)
	}
}

func run() error {
	engine := qml.NewEngine()
	component, err := engine.LoadFile("qml/Main.qml")
	if err != nil {
		return err
	}

	testvar := TestStruct{}
	context := engine.Context()
	context.SetVar("testvar", &testvar)

	win := component.CreateWindow(nil)
	testvar.Root = win.Root()
	win.Show()
	win.Wait()

	return nil
}

type TestStruct struct {
	Root    qml.Object
	TrackingNumber string
	Status string
	ETA string
	AddressFrom string
	AddressTo string
	Progress float64
}

func (testvar *TestStruct) GetMessage(tracker string) {
	c := shippo.NewClient("")
	
	status, err := c.RegisterTrackingWebhook("canada_post", tracker)
	if err != nil {
		log.Fatal(err)
		return
	}

	fmt.Printf("%+v\n", status)

	testvar.TrackingNumber = tracker
	qml.Changed(testvar, &testvar.TrackingNumber)

	switch status.TrackingStatus.Status {
	case "PRE_TRANSIT":
		testvar.Status = "Information Received"
		testvar.Progress = 0.1
	case "TRANSIT":
		testvar.Status = "In Transit"
		testvar.Progress = 0.5
	case "DELIVERED":
		testvar.Status = "Delivered!"
		testvar.Progress = 1.0
	case "UNKNOWN":
		testvar.Status = "Unknown"
		testvar.Progress = 0.0
	case "RETURNED":
		testvar.Status = "Returned/Unclaimed"
		testvar.Progress = 0.0
	case "FAILURE":
		testvar.Status = "Lost/Disposed"
		testvar.Progress = 0.0
	}
	qml.Changed(testvar, &testvar.Status)
	qml.Changed(testvar, &testvar.Progress)

	testvar.ETA = status.ETA.Format("January 2")
	qml.Changed(testvar, &testvar.ETA)

	if status.AddressFrom == nil {
		testvar.AddressFrom = ""
	} else {
		testvar.AddressFrom = status.AddressFrom.City + status.AddressFrom.State
	}
	qml.Changed(testvar, &testvar.AddressFrom)

	if status.AddressTo == nil {
		testvar.AddressTo = ""
	} else {
		testvar.AddressTo = status.AddressTo.City + status.AddressTo.State
	}
	qml.Changed(testvar, &testvar.AddressTo)
}
