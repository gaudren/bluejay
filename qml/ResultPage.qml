import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.Controls.Material 2.0

Page { 
        property string tracking
        onTrackingChanged: testvar.getMessage(tracking)

        header: BlueJayHeader {

        }
    
        ColumnLayout {
            spacing: units.gu(2)
            anchors {
                margins: units.gu(2)
                horizontalCenter: parent.horizontalCenter
                top: parent.top
                left: parent.left
                right: parent.right
            }
            
            Label {
                text: testvar.trackingNumber
                opacity: 0.5
                font.pointSize: 12
                horizontalAlignment: Qt.AlignHCenter
                Layout.fillWidth: true 
            }

            Label {
                text: testvar.status
                color: 'green'
                font.pointSize: 22
                horizontalAlignment: Qt.AlignHCenter
                Layout.fillWidth: true
            }

            Label {
                text: testvar.eta
                font.pointSize: 8
                horizontalAlignment: Qt.AlignHCenter
                Layout.fillWidth: true
            }

            ProgressBar {
                value: testvar.progress
                Layout.fillWidth: true
            }

            RowLayout {
                Layout.fillWidth: true

                Label {
                    text: testvar.addressFrom
                    font.pointSize: 8
                    opacity: 0.5
                    horizontalAlignment: Qt.AlignLeft
                    Layout.alignment: Qt.AlignLeft
                }

                Label {
                    text: testvar.addressTo
                    font.pointSize: 8
                    opacity: 0.5
                    horizontalAlignment: Qt.AlignRight
                    Layout.alignment: Qt.AlignRight
                    Layout.fillWidth: true
                }
            }

            Label {
                Layout.topMargin: units.gu(2)
                text: 'Delivery Status'
                font.pointSize: 22
                horizontalAlignment: Qt.AlignLeft
                Layout.alignment: Qt.AlignLeft
            }

        }
    }