import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.Controls.Material 2.0

Page {
        header: BlueJayHeader {

        }
    
        ColumnLayout {
            spacing: units.gu(2)
            anchors {
                margins: units.gu(2)
                horizontalCenter: parent.horizontalCenter
                verticalCenter: parent.verticalCenter
                left: parent.left
                right: parent.right
            }

            TextField {
                id: tracking
                placeholderText: 'Tracking #'
                horizontalAlignment: TextInput.AlignHCenter
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignCenter
            }

            ComboBox {
                model: ['Testing']
                Layout.preferredWidth: tracking.width * 0.8
                Layout.alignment: Qt.AlignCenter
            } 

        }


        Button {
            text: 'Go'
            onClicked: pageStack.push('ResultPage.qml', {
                "tracking": tracking.text
            })

            anchors {
                bottom: parent.bottom
                horizontalCenter: parent.horizontalCenter
            }
        }
    }