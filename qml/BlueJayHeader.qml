import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.Controls.Material 2.0

ToolBar {
    id: header

    Settings {
        property alias darkMode: tSwitch.checked
    } 

    Rectangle {
        id: headercolor
        anchors.fill: parent
        color: '#F5F5F5'
        RowLayout {
            anchors {
                fill: parent
                rightMargin: units.gu(2)
            }

            Label {
                id: title
                text: 'BlueJay'
                color: '#000000'
                font.pointSize: 18
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignLeft
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
                Layout.leftMargin: units.gu(2)
                visible: pageStack.depth === 1
            }

            ToolButton {
                text: '‹'
                onClicked: pageStack.pop()
                visible: pageStack.depth != 1
            }

            Item {
                Layout.fillWidth: true
            }

            Switch {
                id: tSwitch
                onCheckedChanged: if (checked) {
                    root.Material.theme = Material.Dark;
                    headercolor.color = '#333333';
                    title.color = '#EEEEEE';
                } else {
                    root.Material.theme = Material.Light;
                    headercolor.color = '#F5F5F5';
                    title.color = '#000000';
                }
            }
        }
    }
}