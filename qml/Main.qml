import QtQuick 2.7
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import QtQuick.Controls.Material 2.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'bluejay.whateveryouwantitdoesntreallymatter'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    function trackingReceived(details) {
        pageStack.push('ResultPage.qml', {
            'details': details
        });
    }

    Component {
        id: mainComponent
    
        MainPage {
            id: mainPage
        }
    }

    StackView {
        id: pageStack
        initialItem: mainComponent
        anchors.fill: parent
    }

}
