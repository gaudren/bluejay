module bluejay

go 1.13

require (
	github.com/coldbrewcloud/go-shippo v1.5.0
	github.com/nanu-c/qml-go v0.0.0-20200909205931-8c61a229e64c
)
